<?php

use geeks4change\composable_inheritance\ComposableInheritanceAutoloader;

spl_autoload_register([ComposableInheritanceAutoloader::class, 'staticAutoload']);
