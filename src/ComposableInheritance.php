<?php

namespace geeks4change\composable_inheritance;

/**
 * API class to create composable-inheritance class names.
 */
final class ComposableInheritance {

  /**
   * @var string
   */
  private $class;

  /**
   * Constructor for use with ::class().
   *
   * @param string $class
   *
   * @return \geeks4change\composable_inheritance\ComposableInheritance
   */
  public static function create(string $class): ComposableInheritance {
    return new self($class);
  }

  /**
   * Constructor to alter class by reference.
   *
   * @param string &$class
   *
   * @return \geeks4change\composable_inheritance\ComposableInheritance
   */
  public static function alter(string &$class): ComposableInheritance {
    return new self($class);
  }

  /**
   * ComposableInheritance constructor.
   *
   * @param string &$class
   */
  private function __construct(string &$class) {
    $this->class = &$class;
  }

  /**
   * Use a trait.
   *
   * @param string $trait
   *
   * @return $this
   */
  public function useTrait(string $trait): ComposableInheritance {
    $this->class .= '\\__use\\' . trim($trait, '\\');
    return $this;
  }

  /**
   * Inherit an interface.
   *
   * @param string $interface
   *
   * @return $this
   */
  public function implementsInterface(string $interface): ComposableInheritance {
    $this->class .= '\\__implements\\' . trim($interface, '\\');
    return $this;
  }

  /**
   * Get the final class FQN.
   *
   * @return string
   */
  public function class(): string {
    return $this->class;
  }

}
