<?php

namespace geeks4change\composable_inheritance;

/**
 * Class ComposableInheritanceAutoloader
 */
final class ComposableInheritanceAutoloader {

  /**
   * Static autoload callback.
   *
   * We do that static / instance voodoo to cache the work in ::createInstance.
   *
   * @param $fqn
   *   The class FQN.
   *
   * @internal
   */
  public static function staticAutoload($fqn) {
    self::getInstance()->autoload($fqn);
  }

  private static $instance;

  /**
   * Get singleton instance.
   *
   * @return \geeks4change\composable_inheritance\ComposableInheritanceAutoloader
   *
   * @internal
   */
  private static function getInstance(): ComposableInheritanceAutoloader {
    if (!isset(self::$instance)) {
      self::$instance = self::createInstance();
    }
    return self::$instance;
  }

  /**
   * Create instance.
   *
   * @return \geeks4change\composable_inheritance\ComposableInheritanceAutoloader
   */
  private static function createInstance(): ComposableInheritanceAutoloader {
    if ($dir = $_SERVER['COMPOSABLE_INHERITANCE_CLASS_STORAGE_DIRECTORY'] ?? null) {
      $loader = new FileCodeLoader($dir);
    }
    else {
      $loader = new EvalCodeLoader();
    }
    return new self($loader);
  }

  /**
   * The code loader.
   *
   * @var \geeks4change\composable_inheritance\CodeLoaderInterface
   */
  private $loader;

  /**
   * ComposableInheritanceAutoloader constructor.
   *
   * @param \geeks4change\composable_inheritance\CodeLoaderInterface $loader
   */
  private function __construct(CodeLoaderInterface $loader) {
    $this->loader = $loader;
  }

  /**
   * Autoload.
   *
   * @param $fqn
   *   The class FQN.
   *
   * @internal
   */
  public function autoload($fqn) {
    if (preg_match('/^(?<base_class>.*)(?<glue>\\\\__(?<what>use|implements)\\\\)(?<new_ns>.*?)(?<new_name>[^\\\\]*?)$/u', $fqn, $matches)) {
      try {
        $baseClass = $matches['base_class'];
        $newThing = $matches['new_ns'] . $matches['new_name'];
        $fullNs = rtrim($matches['base_class'] . $matches['glue'] . $matches['new_ns'], '\\');
        $fullClass = $matches['new_name'];
        $what = $matches['what'];
        if (!$this->loader) {
          $this->loader = new EvalCodeLoader();
        }

        if (
          'use' === $what
          && class_exists($baseClass)
          && trait_exists($newThing)
        ) {
          $code = "namespace $fullNs; class $fullClass extends \\$baseClass { use \\$newThing; }";
          $this->loader->load($code);
        }

        elseif (
          'implements' === $what
          && class_exists($baseClass)
          && interface_exists($newThing)
        ) {
          $code = "namespace $fullNs; class $fullClass extends \\$baseClass implements \\$newThing {}";
          $this->loader->load($code);
        }

      } catch (\Throwable $e) {
        $msg = "Error autoloading $fqn\n\n" . var_export(get_defined_vars(), TRUE);
        throw new \RuntimeException($msg, 0, $e);
      }

    }
  }

}
