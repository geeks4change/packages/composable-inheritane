<?php

namespace geeks4change\composable_inheritance;

class EvalCodeLoader implements CodeLoaderInterface {

  public function load(string $code) {
    eval($code);
  }

}
