<?php

namespace geeks4change\composable_inheritance;


class FileCodeLoader implements CodeLoaderInterface {

  /**
   * The directory to put the classes in. BEWARE: It must not be in the docroot
   * (as that might open an uploaded-file remote code execution attack vector).
   *
   * @var string
   */
  protected $directory;

  /**
   * FileCodeLoader constructor.
   *
   * @param string $directory
   *   The directory to put the classes in. BEWARE: It must not be in the docroot
   *   (as that might open an uploaded-file remote code execution attack vector).
   */
  public function __construct(string $directory) {
    $this->directory = $directory;
  }

  public function load(string $code) {
    $key = hash('sha256', $code);
    $fileName = "$this->directory/$key.php";
    if (!file_exists($fileName)) {
      $tmpFileName = tempnam($this->directory, $key);
      file_put_contents($tmpFileName, $code);
      rename($tmpFileName, $fileName);
    }
    require $fileName;
  }

}
