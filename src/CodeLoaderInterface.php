<?php

namespace geeks4change\composable_inheritance;

interface CodeLoaderInterface {

  /**
   * Load code, maybe hashed and OPCached.
   *
   * @param string $code
   *   The code.
   */
  public function load(string $code);

}
