<?php

namespace geeks4change\composable_inheritance\Tests;

use geeks4change\composable_inheritance\ComposableInheritance;
use geeks4change\composable_inheritance\Examples\BarTrait;
use geeks4change\composable_inheritance\Examples\BazTrait;
use geeks4change\composable_inheritance\Examples\Foo;
use geeks4change\composable_inheritance\Examples\FooInterface;
use PHPUnit\Framework\TestCase;

class ComposableInheritanceAutoloaderTest extends TestCase {

  /**
   * @dataProvider autoloadProvider
   */
  public function testAutoload(string $class, string $expectedResult, array $expectedInterfaces) {
    $this->assertEquals($expectedInterfaces, array_values(class_implements($class)));

    $result = $class::hello();
    $this->assertEquals($expectedResult, $result);
  }

  public function autoloadProvider() {
    require_once __DIR__ . '/Fixtures/Foo.php';
    require_once __DIR__ . '/Fixtures/BarTrait.php';
    require_once __DIR__ . '/Fixtures/BazTrait.php';
    require_once __DIR__ . '/Fixtures/FooInterface.php';

    $fooBarClass = Foo::class;
    ComposableInheritance::alter($fooBarClass)
      ->useTrait(BarTrait::class);
    return [
      [
        $fooBarClass,
        'Hello!Bar!',
        [],
      ],
      [
        ComposableInheritance::create(Foo::class)
          ->useTrait(BarTrait::class)
          ->class(),
        'Hello!Bar!',
        [],
      ],
      [
        ComposableInheritance::create(Foo::class)
          ->useTrait(BarTrait::class)
          ->useTrait(BarTrait::class)
          ->class(),
        'Hello!Bar!Bar!',
        [],
      ],
      [
        ComposableInheritance::create(Foo::class)
          ->useTrait(BarTrait::class)
          ->useTrait(BazTrait::class)
          ->class(),
        'Hello!Bar!Baz!',
        [],
      ],
      [
        ComposableInheritance::create(Foo::class)
          ->useTrait(BazTrait::class)
          ->useTrait(BarTrait::class)
          ->class(),
        'Hello!Baz!Bar!',
        [],
      ],
      [
        ComposableInheritance::create(Foo::class)
          ->implementsInterface(FooInterface::class)
          ->class(),
        'Hello!',
        [FooInterface::class],
      ],
      [
        ComposableInheritance::create(Foo::class)
          ->useTrait(BarTrait::class)
          ->implementsInterface(FooInterface::class)
          ->class(),
        'Hello!Bar!',
        [FooInterface::class],
      ],
      [
        ComposableInheritance::create(Foo::class)
          ->implementsInterface(FooInterface::class)
          ->useTrait(BarTrait::class)
          ->class(),
        'Hello!Bar!',
        [FooInterface::class],
      ],
    ];
  }

}
