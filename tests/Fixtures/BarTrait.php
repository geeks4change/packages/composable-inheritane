<?php

namespace geeks4change\composable_inheritance\Examples;

trait BarTrait {

  public static function hello() {
    return parent::hello() . 'Bar!';
  }

}
