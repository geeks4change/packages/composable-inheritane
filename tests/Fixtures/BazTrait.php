<?php

namespace geeks4change\composable_inheritance\Examples;

trait BazTrait {

  public static function hello() {
    return parent::hello() . 'Baz!';
  }

}
